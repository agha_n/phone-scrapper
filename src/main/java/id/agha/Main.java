package id.agha;

import id.agha.service.HandphoneScrapper;
import id.agha.service.HandphoneScrapperImpl;

public class Main {

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "chromedriver");
        HandphoneScrapper handphoneScrapper = new HandphoneScrapperImpl();
        try {
            handphoneScrapper.extractProductToCSV(handphoneScrapper.getListProduct(100));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
