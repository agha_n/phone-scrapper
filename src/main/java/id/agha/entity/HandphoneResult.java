package id.agha.entity;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class HandphoneResult {

    private String productName;
    private String description;
    private String imageLink;
    private String price;
    private String rating;
    private String store;

}
