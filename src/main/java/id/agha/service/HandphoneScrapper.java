package id.agha.service;

import id.agha.entity.HandphoneResult;

import java.util.List;

public interface HandphoneScrapper {

    List<HandphoneResult> getListProduct(int limit);
    void extractProductToCSV(List<HandphoneResult> products) throws Exception;

}
