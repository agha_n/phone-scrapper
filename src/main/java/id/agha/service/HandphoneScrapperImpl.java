package id.agha.service;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import id.agha.entity.HandphoneResult;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class HandphoneScrapperImpl implements HandphoneScrapper {

    @Override
    public List<HandphoneResult> getListProduct(int limit){
        List<WebElement> webElements = new ArrayList<>();
        List<HandphoneResult> products = new ArrayList<>();
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        WebDriver driver = new ChromeDriver(options);
        int page = 1;
        while (webElements.size() < limit) {
            List<WebElement> temp = retrieveElements(driver, page);
            synchronized (temp) {
                webElements.addAll(temp);
                List<HandphoneResult> handphoneResultList = parseProduct(driver, temp);
                products.addAll(handphoneResultList);
                page++;
            }
        }

        driver.close();
        driver.quit();
        return products.subList(0, limit);
    }

    @Override
    public void extractProductToCSV(List<HandphoneResult> handphoneResults) throws Exception {
        if (!handphoneResults.isEmpty()) {
            Path path = Path.of("output.csv");
            try(Writer writer = new FileWriter(path.toString())) {
                StatefulBeanToCsv<HandphoneResult> beanToCsv = new StatefulBeanToCsvBuilder<HandphoneResult>(writer)
                        .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                        .build();
                beanToCsv.write(handphoneResults);
            }
        } else {
            throw new UnsupportedOperationException("received product entities with size 0");
        }
    }

    private List<WebElement> retrieveElements(WebDriver driver, int page){
        List<WebElement> elements = new ArrayList<>();
        try {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            driver.manage().window().maximize();
            driver.get("https://www.tokopedia.com/p/handphone-tablet/handphone?page=" + page);
            ((JavascriptExecutor)driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");
            executor.executeScript("return document.getElementById('zeus-header').remove()");
            WebElement categoryListContainer = driver.findElement(By.xpath("//*[@data-testid='lstCL2ProductList']"));
            List<WebElement> temp = categoryListContainer.findElements(By.xpath(".//*[@data-testid='lnkProductContainer']"));
            int current = temp.size() - 1;
            do {
                executor.executeScript("arguments[0].scrollIntoView(true);", temp.get(current));
                elements = temp;
                temp = categoryListContainer.findElements(By.xpath(".//*[@data-testid='lnkProductContainer']"));
                current += 10;
            } while (current < temp.size());
        } catch (Exception e) {
            driver.close();
            e.printStackTrace();
        }

        return elements;
    }

    private List<HandphoneResult> parseProduct(WebDriver driver, List<WebElement> elements){
        List<HandphoneResult> productEntities = new ArrayList<>();
        for (WebElement element : elements) {
            HandphoneResult product = new HandphoneResult();
            WebElement imageUrl = element.findElement(By.xpath(".//img"));
            WebElement productName = element.findElement(By.xpath(".//span[@class='css-1bjwylw']"));
            WebElement storeName = element.findElements(By.xpath(".//span[@class='css-1kr22w3']")).get(1);
            WebElement price = element.findElement(By.xpath(".//span[@class='css-o5uqvq']"));
            product.setImageLink(imageUrl.getAttribute("src"));
            product.setProductName(productName.getText());
            product.setStore(storeName.getAttribute("innerHTML"));
            product.setPrice(price.getText());
//            getDescriptionAndRating(driver, element, product);
            productEntities.add(product);
        }
        return productEntities;
    }

    private void getDescriptionAndRating(WebDriver driver, WebElement detail, HandphoneResult product){
        detail.click();
        ((JavascriptExecutor)driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");
        String description = driver.findElement(By.xpath("//*[@data-testid='lblPDPDescriptionProduk']")).getAttribute("innerHTML");
        String rating = driver.findElement(By.xpath("//*[@data-testid='lblPDPDetailProductRatingNumber']")).getAttribute("innerHTML");
        product.setDescription(description);
        product.setRating(rating);
    }
}
