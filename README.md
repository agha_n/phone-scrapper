# phone-scraper

Phone Scraper is a utility tool to extract handphone product data from Tokopedia and store it as CSV file.

## Requirement
* Java 11
* Maven
* [Chrome Driver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

## How to run
Execute through the main function inside the Main.java. 
CSV file will generated as output.csv in project directory
